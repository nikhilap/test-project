FROM nginx:1.17.1-alpine
WORKDIR /app
COPY nginx.conf /etc/nginx/nginx.conf
COPY dist/charts dist
